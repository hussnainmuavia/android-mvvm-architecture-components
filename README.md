# Android MVVM Architecture Component With Dagger in Kotlin

I have included some of the libraries that we use in our every day work but you're in **no way** required to use them. I've also added Unit & Integration Tests

## Application Overview

#### User List

When opening the app, It shows a list of all Kotlin developers on Git.

* Used the GitHub API to load the users
* You can either use the [v3](https://developer.github.com/v3/search/#search-users) or [v4](https://developer.github.com/v4/object/user/) version of their API
* Load maximum 10 users at once
* Only shows Kotlin users
* Each displayed user item contain:
* username
* avatar
*  order by username

All of the network related code has implemented in the separate network module.

#### Application Detail

When clicking on a user item, It shows a detail screen.

* A mandatory detail is the email address as well as the number of followers
* When clicking on the email address, the default email app, installed on the device should open up

## Contact

If you have any further question please do not hesitate to ask [Hussnain Muavia](mailto:hussnain.muavia@gmail.com).
