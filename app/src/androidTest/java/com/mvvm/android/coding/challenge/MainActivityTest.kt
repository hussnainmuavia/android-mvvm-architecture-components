package com.mvvm.android.coding.challenge

import androidx.test.rule.ActivityTestRule
import com.mvvm.android.coding.challenge.feature.main.MainActivity
import org.junit.Rule
import org.junit.Test

class MainActivityTest {
  @get:Rule val activityRule = ActivityTestRule(MainActivity::class.java)

  @Test fun sampleTest() {
    // Only a sample test which you can adjust and to get you started easily.
  }
}
