package com.mvvm.android.coding.challenge.feature.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mvvm.android.coding.challenge.R
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mvvm.android.coding.challenge.AndroidApplication
import com.mvvm.android.coding.challenge.core.di.ApplicationComponent
import com.mvvm.android.coding.challenge.core.utils.Constants
import com.mvvm.android.coding.challenge.feature.viewmodels.UserDetailViewModel
import com.mvvm.android.coding.challenge.network.models.UserDetails
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_user_detail.*
import kotlinx.android.synthetic.main.fragment_user_detail.ivAvatar
import javax.inject.Inject
import java.text.SimpleDateFormat


class UserDetailFragment : androidx.fragment.app.Fragment() {

    private var email: String = ""

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent
    }

    val userViewModel by lazy { ViewModelProviders.of(this, viewModelFactory)[UserDetailViewModel::class.java] }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_user_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appComponent.inject(this)

        val bundle = this.arguments
        val userModel = bundle!!.get(Constants.ITEM_KEY)

        userViewModel.getUserDetails(userModel.toString())?.observe(this, Observer {
            initViews(it.copy())
        })
    }

    private fun initViews(it: UserDetails) {

        Picasso.get().load(it.avatarUrl).into(ivAvatar)

        if (it.name != null) {
            tvName.text = it.name.toString()
        } else {
            tvName.text = Constants.N_A
        }

        if (it.login != null) {
            tvUsername.text = it.login.toString()
        } else {
            tvUsername.text = Constants.N_A
        }

        if (it.publicRepos != null) {
            tvRepositeriesValue.text = it.publicRepos.toString()
        } else {
            tvRepositeriesValue.text = Constants.N_A
        }

        if (it.followers != null) {
            tvFollowersValue.text = it.followers.toString()
        } else {
            tvFollowersValue.text = Constants.N_A
        }

        if (it.following != null) {
            tvFollowingValue.text = it.following.toString()
        } else {
            tvFollowingValue.text = Constants.N_A
        }

        if (it.bio != null) {
            tvBioValue.text = it.bio.toString()
        } else {
            tvBioValue.text = Constants.N_A
        }

        if (it.createdAt != null) {
            tvJoinedAtValue.text = formatDate(it.createdAt.toString())

        } else {
            tvJoinedAtValue.text = Constants.N_A
        }

        if (it.email != null) {
            email = it.email.toString()
            tvEmailValue.text = email

            emailTextViewClick()

        } else {
            tvEmailValue.text = Constants.N_A
        }

        if (it.company != null) {
            tvCompanyValue.text = it.company.toString()
        } else {
            tvCompanyValue.text = Constants.N_A
        }

        if (it.location != null) {
            tvLocationValue.text = it.location.toString()
        } else {
            tvLocationValue.text = Constants.N_A
        }

        if (it.blog != null) {
            tvBlogValue.text = it.blog.toString()
        } else {
            tvBlogValue.text = Constants.N_A
        }
    }

    private fun emailTextViewClick(){
        tvEmailValue.setOnClickListener {
            userViewModel.sendEmailViaApp(email, context!!)
        }
    }

    private fun formatDate(it: String): String {
        val parser = SimpleDateFormat(Constants.DATE_FORMAT)
        val formatter = SimpleDateFormat(Constants.DATE)
        return formatter.format(parser.parse(it))
    }
}
