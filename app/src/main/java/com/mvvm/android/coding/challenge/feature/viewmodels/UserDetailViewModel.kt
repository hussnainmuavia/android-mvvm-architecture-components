package com.mvvm.android.coding.challenge.feature.viewmodels

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.core.api.ApiInterface
import com.mvvm.android.coding.challenge.core.utils.EmailConstants
import com.mvvm.android.coding.challenge.network.models.UserDetails
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class UserDetailViewModel @Inject constructor(private val apiInterface: ApiInterface) : ViewModel()  {

    var usersLiveData: MutableLiveData<UserDetails>? = null

    private val compositeDisposable = CompositeDisposable()

    fun get(username: String) =
            compositeDisposable.add(apiInterface.getUserDetails(username)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleResponse, this::handleError))

    private fun handleResponse(usersList: UserDetails){
        usersLiveData!!.value = usersList
    }

    private fun handleError(error: Throwable){
        Log.d("API ERROR", error.toString())
    }

    fun getUserDetails(username: String): LiveData<UserDetails>? {

        if (usersLiveData == null) {
            this.usersLiveData = MutableLiveData()
            get(username)
        }
        return this.usersLiveData
    }

    fun sendEmailViaApp(email: String, context: Context){
        val emailIntent = Intent(Intent.ACTION_SEND);
        emailIntent.setType(EmailConstants.SET_TYPES);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, EmailConstants.SUBJECT);
        emailIntent.putExtra(Intent.EXTRA_TEXT, EmailConstants.TEXT);
        context.startActivity(Intent.createChooser(emailIntent, EmailConstants.TITLE));
    }
}