package com.mvvm.android.coding.challenge.feature.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.mvvm.android.coding.challenge.R
import com.mvvm.android.coding.challenge.feature.callbacks.RecyclerViewItemClickListener
import com.mvvm.android.coding.challenge.network.models.UserModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_row.view.*


class RecyclerViewAdapter(val items: UserModel, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    private var recyclerClickListener: RecyclerViewItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, positon: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_row, parent, false))
    }

    fun onCallbacksClickListener(recyclerListener: RecyclerViewItemClickListener) {
        this.recyclerClickListener = recyclerListener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvUsername.text = items.items!![position].login!!
        val uri: Uri? = items.items!![position].avatarUrl?.toUri()
        Picasso.get().load(uri).into(holder.ivAvatar)
        holder.itemView.setOnClickListener {
            this.recyclerClickListener!!.onUserItemClickClickListener(position, items.items!![position].login.toString())
        }
    }

    override fun getItemCount(): Int {
        return items.items!!.size
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val tvUsername = view.tvUsername!!
    val ivAvatar = view.ivAvatar!!
}