package com.mvvm.android.coding.challenge.feature.callbacks

interface RecyclerViewItemClickListener {

    fun onUserItemClickClickListener(position: Int, username: String)
}