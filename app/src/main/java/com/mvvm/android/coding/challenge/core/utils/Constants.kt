package com.mvvm.android.coding.challenge.core.utils

import com.mvvm.android.coding.challenge.BuildConfig

object Constants {

    const val APP_VERSION = BuildConfig.VERSION_NAME
    const val EMPTY_STRING = ""
    const val ITEM_KEY = "item_selected_key"
    const val N_A = "N/A"
    const val API_ERROR = "API ERROR"
    const val WI_FI = "Wi-Fi"
    const val DATA = "Data Service"
    const val DATE_FORMAT = "yyyy-MM-dd"
    const val DATE = "dd-MM-yyyy"
}

object ApiEndPoints {

    const val BASE_URL = "https://api.github.com/"
}

object EmailConstants {

    const val SET_TYPES = "plain/text"
    const val SUBJECT = "Subject"
    const val TEXT = "Text"
    const val TITLE = "Send Mail..."
}