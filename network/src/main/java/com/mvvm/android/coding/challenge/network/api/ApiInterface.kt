package com.android.core.api

import com.mvvm.android.coding.challenge.network.models.UserDetails
import com.mvvm.android.coding.challenge.network.models.UserModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("/search/users")
    fun getUsersData(@Query("q") q: String, @Query("page") page: String, @Query("per_page") perPage: String, @Query("order") order: String): Observable<UserModel>

    @Headers("Authorization: Bearer YourOAuthAccessTokenWillBePlaceHere")
    @GET("/users/{username}")
    fun getUserDetails(@Path(value = "username") userId: String): Observable<UserDetails>
}