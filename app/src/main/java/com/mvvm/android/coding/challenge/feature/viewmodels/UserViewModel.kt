package com.mvvm.android.coding.challenge.feature.viewmodels


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.core.api.ApiInterface
import com.mvvm.android.coding.challenge.core.utils.Constants
import com.mvvm.android.coding.challenge.network.models.UserModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class UserViewModel @Inject constructor(private val apiInterface: ApiInterface) : ViewModel() {

    private var progress: Boolean = true
    private var usersLiveData: MutableLiveData<UserModel>? = null

    private val compositeDisposable = CompositeDisposable()

    private fun get(q: String, page: String, perPage: String, order: String) =
            compositeDisposable.add(apiInterface.getUsersData(q, page, perPage, order)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleResponse, this::handleError))

    private fun handleResponse(usersList: UserModel) {
            usersLiveData!!.value = usersList
            setProgress(false)
    }

    private fun handleError(error: Throwable) {
        Log.d(Constants.API_ERROR, error.toString())
        setProgress(true)
    }

    fun getUsersData(q: String, page: String, perPage: String, order: String): LiveData<UserModel> {

        if (usersLiveData == null) {
            this.usersLiveData = MutableLiveData<UserModel>()
        }
        setProgress(false)

        get(q, page, perPage, order)
        return usersLiveData!!
    }

    private fun setProgress(progress: Boolean) {
        this.progress = progress
    }

    fun getProgress(): Boolean {
        return this.progress
    }
}