package com.mvvm.android.coding.challenge.feature.main

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.mvvm.android.coding.challenge.R
import com.mvvm.android.coding.challenge.feature.fragments.MainFragment
import android.view.MenuItem
import com.mvvm.android.coding.challenge.core.utils.Utils
import com.mvvm.android.coding.challenge.feature.fragments.AboutDialogFragment


class MainActivity : AppCompatActivity() {

    private var mSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (Utils.isNetworkConnected(this@MainActivity))
            showSnackBar(resources.getString(R.string.connected_to, "${Utils.getNetworkSource()}"))
        else
            showSnackBar(resources.getString(R.string.no_internet))


        mainFragment()
    }

    private fun showSnackBar(message: String) {

        mSnackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
        mSnackbar?.run {
            show()
        }
    }

    private fun mainFragment() {

        val mainFragment = MainFragment()
        val transaction = supportFragmentManager!!.beginTransaction()
        transaction.replace(R.id.fragmentContainer, mainFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_about -> {

                showAboutDialogFragment()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showAboutDialogFragment() {

        var fm = getSupportFragmentManager();
        var editNameDialogFragment = AboutDialogFragment()
        editNameDialogFragment.show(fm, editNameDialogFragment.tag);
    }
}
