package com.mvvm.android.coding.challenge


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.mvvm.android.coding.challenge.feature.main.MainActivity
import com.mvvm.android.coding.challenge.feature.main.SplashActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainFragmentToDetailsFragmentTest {

    @Rule
    @JvmField
    var mActivitySplashTestRule = ActivityTestRule(SplashActivity::class.java)

    @Rule
    @JvmField
    var mActivityMainTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainFragmentToDetailsFragmentTest() {
        val constraintLayout = onView(
                childAtPosition(
                        allOf(withId(R.id.recyclerView),
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        0)),
                        4))
        constraintLayout.perform(scrollTo(), click())

        pressBack()

        val constraintLayoutDetails = onView(
                childAtPosition(
                        allOf(withId(R.id.recyclerView),
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        0)),
                        2))
        constraintLayoutDetails.perform(scrollTo(), click())

        val appTvForEmailView = onView(
                allOf(withId(R.id.tvEmailValue), withText("marcinmoskala@gmail.com"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.ScrollView")),
                                        0),
                                16)))
        appTvForEmailView.perform(scrollTo(), click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
