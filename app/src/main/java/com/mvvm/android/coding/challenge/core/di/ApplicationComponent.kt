package com.mvvm.android.coding.challenge.core.di

import com.mvvm.android.coding.challenge.AndroidApplication
import com.android.core.api.ApiModule
import com.mvvm.android.coding.challenge.core.di.viewmodel.ViewModelModule
import com.mvvm.android.coding.challenge.feature.fragments.MainFragment
import com.mvvm.android.coding.challenge.feature.fragments.UserDetailFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, ViewModelModule::class])
interface ApplicationComponent {

    fun inject(application: AndroidApplication)
    fun inject(mainFragment: MainFragment)
    fun inject(userDetailFragment: UserDetailFragment)
}
