package com.mvvm.android.coding.challenge.feature.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mvvm.android.coding.challenge.AndroidApplication
import com.mvvm.android.coding.challenge.R
import com.mvvm.android.coding.challenge.core.di.ApplicationComponent
import com.mvvm.android.coding.challenge.core.utils.Constants
import com.mvvm.android.coding.challenge.feature.adapters.RecyclerViewAdapter
import com.mvvm.android.coding.challenge.feature.callbacks.RecyclerViewItemClickListener
import com.mvvm.android.coding.challenge.feature.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import com.mvvm.android.coding.challenge.network.models.UserModel
import javax.inject.Inject


class MainFragment : androidx.fragment.app.Fragment(), RecyclerViewItemClickListener {

    private lateinit var recyclerViewAdapter: RecyclerViewAdapter
    private var nextData: Int = 10
    private var items: UserModel? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent
    }

    private val userViewModel by lazy { ViewModelProviders.of(this, viewModelFactory)[UserViewModel::class.java] }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appComponent.inject(this)

        progressBar.isIndeterminate

        recyclerView.layoutManager = LinearLayoutManager(context)

        userViewModel.getUsersData("language:kotlin", "1", "" + nextData, "asc").observe(this, Observer {

            recyclerViewAdapter = RecyclerViewAdapter(it, this@MainFragment.requireContext())
            recyclerView.adapter = recyclerViewAdapter
            recyclerViewAdapter.onCallbacksClickListener(this@MainFragment)

            setProgressBarVisibility()

            recyclerViewAddOnScroll()

        })
    }

    private fun setProgressBarVisibility() {

        if (userViewModel.getProgress()) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    private fun recyclerViewAddOnScroll() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy >= 0) {
                    fetchDataFromViewModel()
                }
            }
        })
    }

    private fun fetchDataFromViewModel() {
        nextData += 10

        userViewModel.getUsersData("language:kotlin", "1", "" + nextData, "asc").observe(this, Observer {
            if(it != null) {
                items = it
                recyclerView.adapter!!.notifyDataSetChanged()
            }
        })
    }

    override fun onUserItemClickClickListener(position: Int, username: String) {

        var mBundle = Bundle()
        mBundle.putString(Constants.ITEM_KEY, username)

        val userDetailFragment = UserDetailFragment()
        userDetailFragment.setArguments(mBundle)
        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(R.id.fragmentContainer, userDetailFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
