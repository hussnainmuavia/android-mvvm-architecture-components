package com.mvvm.android.coding.challenge.core.utils

import android.content.Context
import android.net.ConnectivityManager

object Utils {

    private var source : String = ""

    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo

        if (activeNetwork != null) {
            when (activeNetwork.type) {
                ConnectivityManager.TYPE_WIFI -> source = Constants.WI_FI
                ConnectivityManager.TYPE_MOBILE -> source = Constants.DATA
            }
            return true
        } else {
            return false
        }
    }

    fun getNetworkSource() : String{
        return source
    }
}