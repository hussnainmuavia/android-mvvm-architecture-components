package com.mvvm.android.coding.challenge

import org.junit.Test
import com.android.core.api.ApiModule
import com.mvvm.android.coding.challenge.core.utils.ApiEndPoints

class SampleTest {

    @Test
    fun testAllUsersApiCall(){
        val allUsersApi = ApiModule(ApiEndPoints.BASE_URL).getClient().getUsersData("language:kotlin", "1", "10", "asc")

        allUsersApi.test().assertValue {
            it.items!!.size > 0

            it.items!![0].login != null
            it.items!![0].avatarUrl != null
        }
    }

    @Test
    fun testUserDetailsApiCall() {

        val userDetailApi = ApiModule(ApiEndPoints.BASE_URL).getClient().getUserDetails("hussnainmuavia")

        userDetailApi.test().assertSubscribed()

        userDetailApi.test().assertValue {
            it != null
        }

        userDetailApi.test().assertValue {

            it.login != null
            it.login == "hussnainmuavia"
            it.login.toString().length > 0
            it.login.toString().isNotEmpty()

            it.name != null
            it.name!! == "Hussnain Muavia"
            it.name.toString().length > 0
            it.name.toString().isNotEmpty()

            it.bio != null
            it.bio!!.equals("Software Engineer | Android Developer | Kotlin | Java | Android Enthusiast")
            it.bio.toString().length > 0
            it.bio.toString().isNotEmpty()

            it.createdAt != null
            it.createdAt == "2015-09-05T12:51:28Z"
            it.createdAt.toString().length > 0
            it.createdAt.toString().isNotEmpty()

            it.email != null
            it.email == "hussnain.muavia@gmail.com"
            it.email.toString().length > 0
            it.email.toString().isNotEmpty()

            it.followers != null
            it.followers!! == 0
            it.followers.toString().isNotEmpty()

            it.following != null
            it.following == 0
            it.following.toString().isNotEmpty()

            it.publicRepos != null
            it.publicRepos.toString().length > 0
            it.publicRepos!!.equals("3")
            it.publicRepos.toString().isNotEmpty()

            it.company != null
            it.company.toString().length > 0
            it.company!! == "mobileLIVE"
            it.company.toString().isNotEmpty()

            it.location != null
            it.location.toString().length > 0
            it.location.equals("Lahore, Pakistan")
            it.location.toString().isNotEmpty()
        }
    }
}
