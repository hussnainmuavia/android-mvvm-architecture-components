package com.mvvm.android.coding.challenge

import android.app.Application
import com.android.core.api.ApiModule
import com.mvvm.android.coding.challenge.core.di.ApplicationComponent
import com.mvvm.android.coding.challenge.core.di.DaggerApplicationComponent
import com.mvvm.android.coding.challenge.core.utils.ApiEndPoints

class AndroidApplication : Application() {

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerApplicationComponent
                .builder()
                .apiModule(ApiModule(ApiEndPoints.BASE_URL))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        this.injectMembers()
    }

    private fun injectMembers() = appComponent.inject(this)

    fun getApplicationComponent(): ApplicationComponent {
        return appComponent
    }
}
